﻿using analyst_challenge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace analyst_challenge.Mapper
{
  public static class EventoItemExtensions
  {
    public static EventoItem ToEventoItem(this EventoItemDto evento) => 
      new EventoItem()
      {
        Tag = evento.Tag,
        TimeStamp = evento.TimeStamp,
        Valor = evento.Valor,
        Status = string.IsNullOrEmpty(evento.Valor) ? StatusEnum.Erro : StatusEnum.Sucesso
      };
  }
}
