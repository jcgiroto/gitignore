import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';
import { EventoItem } from './EventoItem';


const API = environment.ApiUrl;

@Injectable({ providedIn: 'root' })
export class FetchdataService
{
  constructor(private http: HttpClient) { }

  listaEvento(id: number): Observable<EventoItem[]> {        
    return this.http
      .get<EventoItem[]>(API + '/ListaEvento/' + id);
  } 
}
