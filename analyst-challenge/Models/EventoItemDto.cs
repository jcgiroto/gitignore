﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace analyst_challenge.Models
{
  public class EventoItemDto
  {
    public long TimeStamp { get; set; }

    public string Tag { get; set; }

    public string Valor { get; set; }
  }
}
