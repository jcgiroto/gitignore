import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

import { FetchdataService } from './fetch-data.Service';
import { EventoItem } from './EventoItem';


///import { interval } from 'rxjs';

import { interval } from "rxjs/internal/observable/interval";
import { startWith, switchMap } from "rxjs/operators";


@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})
export class FetchDataComponent implements OnInit {
   
  public eventos: EventoItem[];

  constructor(private fetchdataService: FetchdataService)
  {
    interval(5000)
      .pipe(
        startWith(0),
        switchMap(() => this.fetchdataService.listaEvento(0))
      )
      .subscribe(result => {
        this.eventos = result;
      }, error => console.error(error));
  }

  ngOnInit(): void {

  }  
}

