"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var environment_1 = require("../../environments/environment");
var API = environment_1.environment.ApiUrl;
//@Injectable({ providedIn: 'root' })
var CounterService = /** @class */ (function () {
    function CounterService(http) {
        this.http = http;
    }
    CounterService.prototype.listaEvento = function () {
        return this.http
            .get(API + '/ListaEvento/');
    };
    return CounterService;
}());
exports.CounterService = CounterService;
//# sourceMappingURL=counter.service.js.map