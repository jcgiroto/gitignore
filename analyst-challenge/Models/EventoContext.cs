﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace analyst_challenge.Models
{  
  public class EventoContext : DbContext
  {
    public EventoContext(DbContextOptions<EventoContext> options)
        : base(options)
    {

    }    

    public DbSet<EventoItem> EventoItems { get; set; }    
  }
}
