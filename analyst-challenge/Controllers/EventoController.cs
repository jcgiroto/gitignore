﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using analyst_challenge.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using analyst_challenge.Mapper;
using System.Linq.Expressions;

namespace analyst_challenge.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class EventoController : ControllerBase
  {    
    private readonly ILogger<EventoController> _logger;
    private readonly EventoContext _context;

    public EventoController(ILogger<EventoController> logger, EventoContext context)
    {
      _context = context;
      _logger = logger;
    }

    [HttpGet("{id}", Name = "GetEvento")]
    public ActionResult<EventoItem> GetById(long id)
    {
      var item = _context.EventoItems.SingleOrDefault(d => d.Id == id);

      if (item == null)      
        return NotFound();      

      return item;
    }    

    [HttpPost]
    [ProducesResponseType(201)]
    [ProducesResponseType(400)]
    public ActionResult<EventoItem> Create([FromBody]EventoItemDto item)
    {
      var eventoItem = item.ToEventoItem();
      _context.EventoItems.Add(eventoItem);
      _context.SaveChanges();

      return CreatedAtRoute("GetEvento", new { id = eventoItem.Id }, item);
    }

  }
}
