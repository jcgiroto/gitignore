﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace analyst_challenge.Models
{
  public class EventoItem
  {    
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public long Id { get; set; }

    public double TimeStamp { get; set; }

    public string Tag { get; set; }

    public string Valor { get; set; }

    public StatusEnum Status { get; set; }

    public string DescricaoStatus => this.Status.ToString();
  }
}
