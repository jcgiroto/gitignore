﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using analyst_challenge.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using analyst_challenge.Mapper;
using System.Linq.Expressions;

namespace analyst_challenge.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class ListaEventoController : ControllerBase
  {
    private readonly ILogger<EventoController> _logger;
    private readonly EventoContext _context;

    public ListaEventoController(ILogger<EventoController> logger, EventoContext context)
    {
      _context = context;
      _logger = logger;
    }

    [HttpGet("{id}", Name = "ListEvento")]
    public ActionResult<List<EventoItem>> ListEvento(long id)
    {
      var itens = _context.EventoItems.Where(d => d.Id > id).ToList();

      if (itens == null)
        return NotFound();

      return itens;
    }

    [HttpGet(Name = "listEventoAgrupado")]
    public ActionResult<List<EventoItem>> ListEvento2()
    {
      var listEventoAgrupado = _context.EventoItems
        .ToList()
        .Where(d => d.Status == (int)StatusEnum.Sucesso)
        .GroupBy(d => d.Tag)
        .Select(
        g => new
          {
            g.Key,
            Valor = g.Sum(s => Convert.ToInt32(s.Valor))
          }
        )
        .ToDictionary(d => d.Key);
      
      var data = new
      {
        labels = listEventoAgrupado.Select(d => d.Key).ToArray(),
        datasets = new[]
        {
          new {
            data = listEventoAgrupado.Select(d => d.Value.Valor).ToArray()
          }            
        }        
      };

      if (data == null)
        return NotFound();

      return Ok(data);
    }
  }
}
