import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Chart } from 'chart.js';
import { CounterService } from './counter.service';


@Component({
  selector: 'app-counter-component',
  templateUrl: './counter.component.html'
})
export class CounterComponent implements OnInit {
  public currentCount = 0;  
  @ViewChild('canvas', { static: false }) canvas?: ElementRef<HTMLElement>;   
  chart: any;
  
  constructor(private counterService: CounterService) {
   
  }

  ngOnInit(): void {    
    this.counterService.listaEvento()
      .subscribe(res => {
        this.chart = new Chart(`canvas`, {
          type: 'horizontalBar',
          data: res,
          options: {
            legend: { display: false },
            title: {
              display: true,
              text: 'Lista de eventos com valor'
            }
          }
        });
      })
  }

  public incrementCounter() {
    this.currentCount++;
  }
}

