﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace analyst_challengeSPA.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class WeatherForecastController : ControllerBase
  {
    private static readonly string[] Summaries = new[]
    {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

    private readonly ILogger<WeatherForecastController> _logger;

    public WeatherForecastController(ILogger<WeatherForecastController> logger)
    {
      _logger = logger;
    }

    [HttpGet]
    public IEnumerable<EventoItem> Get()
    {
      var rng = new Random();
      return Enumerable.Range(1, 5).Select(index => new EventoItem
      {
        Id = 1,
        Tag = "asd",
        TimeStamp = 0,
        Valor = "asd"
      })
      .ToArray();
    }
  }
  public class EventoItem
  {    
    public long Id { get; set; }

    public long TimeStamp { get; set; }

    public string Tag { get; set; }

    public string Valor { get; set; }
  }
}
