import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';

const API = environment.ApiUrl;

@Injectable({ providedIn: 'root' })
export class CounterService {

  constructor(private http: HttpClient) { }

  listaEvento(): Observable<any> {
    return this.http
      .get(API + '/ListaEvento/');
  }
}
