export interface EventoItem
{
  id: number;
  timeStamp: number;
  tag: string;
  valor: string;
  status: string;
  descricaoStatus: string;
}
