﻿using analyst_challenge.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace analyst_challenge
{
  public class DataGenerator
  {
    //TODO: Corrigir
    private static double RandomDay()
    {
      var startDate = new DateTime(1970, 1, 1);
      TimeSpan timeSpan = DateTime.Now - startDate;
      var randomTest = new Random();
      TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, (int)timeSpan.TotalMinutes), 0);

      return newSpan.TotalSeconds;
    }

    public static void Seeded(IServiceProvider serviceProvider)
    {
      using (var context = new EventoContext(serviceProvider.GetRequiredService<DbContextOptions<EventoContext>>()))
      {
        if (context.EventoItems.Any())
        {
          return;
        }

        context.EventoItems.AddRange(
          new EventoItem { Id = 1, Tag = "brasil.sudeste", Valor = "100", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 2, Tag = "brasil.sudeste", Valor = "200", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 3, Tag = "brasil.sudeste", Valor = "300", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 4, Tag = "brasil.sudeste.sensor01", Valor = "2123", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 5, Tag = "brasil.sudeste.sensor01", Valor = "100", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 6, Tag = "brasil.sudeste.sensor01", Valor = "65", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 7, Tag = "brasil.sudeste.sensor01", Valor = "111", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 8, Tag = "brasil.sudeste.sensor02", Valor = "111", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 9, Tag = "brasil.sudeste.sensor02", Valor = "100", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 10, Tag = "brasil.sudeste.sensor02", Valor = "20", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 11, Tag = "brasil.sudeste.sensor02", Valor = "10", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 12, Tag = "brasil.sul", Valor = "1500", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 13, Tag = "brasil.sul", Valor = "3123", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 14, Tag = "brasil.sul", Valor = "324", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 15, Tag = "brasil.sul", Valor = "2344", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 16, Tag = "brasil.sul", Valor = "442", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 17, Tag = "brasil.sul.sensor01", Valor = "442", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 18, Tag = "brasil.sul.sensor01", Valor = "442", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 19, Tag = "brasil.sul.sensor01", Valor = "555", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 21, Tag = "brasil.sul.sensor01", Valor = "442", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 22, Tag = "brasil.sul.sensor01", Valor = "22", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 23, Tag = "brasil.sul.sensor01", Valor = "11", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 24, Tag = "brasil.sul.sensor01", Valor = "111", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 25, Tag = "brasil.sul.sensor01", Valor = "111", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 26, Tag = "brasil.sul.sensor01", Valor = "111", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 27, Tag = "brasil.sul.sensor01", Valor = "111", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 28, Tag = "brasil.sul.sensor01", Valor = "111", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 29, Tag = "brasil.sul.sensor01", Valor = "111", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 30, Tag = "brasil.sul.sensor01", Valor = "111", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 31, Tag = "brasil.sul.sensor02", Valor = "56", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 32, Tag = "brasil.sul.sensor02", Valor = "2323", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 33, Tag = "brasil.sul.sensor02", Valor = "111", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 34, Tag = "brasil.sul.sensor02", Valor = "22", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 35, Tag = "brasil.sul.sensor02", Valor = "44", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 36, Tag = "brasil.sul.sensor02", Valor = "33", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 37, Tag = "brasil.sul.sensor02", Valor = "33", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 38, Tag = "brasil.sul.sensor02", Valor = "33", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 39, Tag = "brasil.sul.sensor02", Valor = "22", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() },
          new EventoItem { Id = 40, Tag = "brasil.sul.sensor02", Valor = "22", Status = StatusEnum.Sucesso, TimeStamp = RandomDay() });

        context.SaveChanges();
      }
    }
  }
}
